#include <MultibjetsAnalysis/ElectronHelper.h>
#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODBase/IParticleHelpers.h"

#include "MultibjetsAnalysis/HelperFunctions.h"

//_________________________________________________________________________
//
ElectronHelper::ElectronHelper(ST::SUSYObjDef_xAOD*& susy_tools, xAOD::TEvent*& event, xAOD::TStore*& store) :
m_event(event),
m_store(store),
m_susy_tools(susy_tools)
{
  m_triggers.clear();
}

//_________________________________________________________________________
// Apply kinematic and quality cuts
void ElectronHelper::DecorateBaselineElectrons(xAOD::ElectronContainer*& electrons, const xAOD::EventInfo * /*info*/, std::string sys_name)
{
  ConstDataVector<xAOD::ElectronContainer> * selected_electrons =  new ConstDataVector<xAOD::ElectronContainer>(SG::VIEW_ELEMENTS);

  for(const auto &elec : *electrons) {
    //trigger matching decorations
    for( const auto trigger : m_triggers ){
      if( trigger.first.find("HLT_e") == std::string::npos ) continue;
      elec->auxdata<char>("MBJ_trigger_matched"+trigger.first) = m_susy_tools -> IsTrigMatched(elec, trigger.first);
    }
    selected_electrons->push_back(elec);
  }
  std::string name = "SelectedElectrons"+sys_name;

  // sort in pT
  std::sort(selected_electrons->begin(), selected_electrons->end(), HelperFunctions::pt_sort());
  if(!m_store->record( selected_electrons, name ).isSuccess()) std::cout << "Could not record selected electrons" << std::endl;
}
