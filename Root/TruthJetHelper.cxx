#include "MultibjetsAnalysis/TruthJetHelper.h"

#include "xAODBase/IParticleHelpers.h"

// P4Helpers
#include <FourMomUtils/xAODP4Helpers.h>

//_________________________________________________________________________
//
TruthJetHelper::TruthJetHelper(xAOD::TEvent*& event, xAOD::TStore*& store) :
  m_event(event),
  m_store(store),
  m_truthjet_copy(0),
  m_truthjet_copy_aux(0),
  m_signal_truth_jets(0),
  m_objKey("AntiKt4TruthJets")
{}

//_________________________________________________________________________
//
TruthJetHelper::TruthJetHelper( const TruthJetHelper &t )
{
    m_event = t.m_event;
    m_truthjet_copy = t.m_truthjet_copy;
    m_truthjet_copy_aux = t.m_truthjet_copy_aux;
    m_objKey = t.m_objKey;
    m_store = t.m_store;
}

//_________________________________________________________________________
//
TruthJetHelper::~TruthJetHelper()
{
  // delete m_truthjet_copy;
  // delete m_truthjet_copy_aux;
  // Clear();
}

//_________________________________________________________________________
//
void TruthJetHelper::RetrieveTruthJets()
{
    //Checks TruthJets container exists
    const xAOD::JetContainer* truth_jets(0);
    if(!m_event->retrieve(truth_jets,m_objKey).isSuccess()){
        Warning( "TruthJetHelper::RetrieveTruthJets()", "Cannot retrieve %s container",m_objKey.c_str() );
        return;
    }

    std::pair<xAOD::JetContainer*,xAOD::ShallowAuxContainer*> jets_shallowcopy = xAOD::shallowCopyContainer(*truth_jets);
    m_truthjet_copy = jets_shallowcopy.first;
    m_truthjet_copy_aux = jets_shallowcopy.second;

    bool setLinks = xAOD::setOriginalObjectLink(*truth_jets, *m_truthjet_copy);
    if(!setLinks) {
        Error("TruthJetHelper::RetrieveTruthJets()","Failed to set original object links.");
    }

    if(!m_store->record(m_truthjet_copy, "TruthJets").isSuccess()) std::cout<< "Could not record Truth Jets" << std::endl;
    if(!m_store->record(m_truthjet_copy_aux, "TruthJetsAux.").isSuccess()) std::cout << "Could not record Truth Jets Aux" << std::endl;

    return;
}

//_________________________________________________________________________
//
void TruthJetHelper::RetrieveSignalTruthJets()
{

  ConstDataVector<xAOD::JetContainer> * selected_jets   =  new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
  ConstDataVector<xAOD::JetContainer> * signal_jets   =  new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
  /* missing in current DAOD
  // Get truth electrons for overlap removal
  const xAOD::TruthParticleContainer* TruthElectrons = 0;
  if(!m_event->retrieve( TruthElectrons, "TruthElectrons").isSuccess()) {
    Warning( "TruthJetHelper::RetrieveSignalTruthJets()", "Cannot retrieve TruthElectrons container");
    return;
  }
  */
  for( const auto jet : *m_truthjet_copy ) {

    selected_jets->push_back(jet);
    //Apply here all the selections
    if(jet->pt() / MEV < 20. ) continue;
    if(fabs(jet->eta())>2.8)   continue;
    /*
    // remove jets overlaping with electrons
    bool passOR = true;
    for( const auto elec : *TruthElectrons ) {
      if(elec->pt() / MEV < 10. ) continue;
      if(fabs(elec->eta())>2.47)   continue;

      //const xAOD::IParticle* IP_jet = static_cast<const xAOD::IParticle*>(jet);
      Float_t dR = xAOD::P4Helpers::deltaR(jet, elec);
      //std::cout << jet->eta() << " " << elec->eta() << " " << dR << std::endl;
      if(dR<0.2) passOR = false;
    }
    if(!passOR) continue;
    */

    //if selection passed: add this object to the vector
    signal_jets->push_back(jet);

  }

  if(!m_store->record(selected_jets, "SelectedTruthJets").isSuccess()) std::cout << "Could not record Selected Truth Jets" << std::endl;
  if(!m_store->record(signal_jets, "SignalTruthJets").isSuccess()) std::cout << "Could not record Selected Truth Jets Aux" << std::endl;

  // Probably not necessary, but doesn't hurt
  std::sort(selected_jets->begin(), selected_jets->end(), HelperFunctions::pt_sort());
  std::sort(signal_jets->begin(), signal_jets->end(), HelperFunctions::pt_sort());

  m_signal_truth_jets = selected_jets->asDataVector();

  return;
}

//_________________________________________________________________________
//
void TruthJetHelper::MakeDeepCopy()
{
  const xAOD::JetContainer* truthjet_container = 0;
  m_event->retrieve( truthjet_container, m_objKey );

  xAOD::JetContainer*    good_truthjet     = new xAOD::JetContainer();
  xAOD::JetAuxContainer* good_truthjet_aux = new xAOD::JetAuxContainer();
  good_truthjet -> setStore( good_truthjet_aux ); //< Connect the two

  xAOD::JetContainer::const_iterator truthjet_itr = (truthjet_container)->begin();
  xAOD::JetContainer::const_iterator truthjet_end = (truthjet_container)->end();

  for( ; truthjet_itr != truthjet_end; ++truthjet_itr ) {
    // Copy this truthjet to the output container
    xAOD::Jet* trj = new xAOD::Jet();
    trj->makePrivateStore( **truthjet_itr );
    good_truthjet->push_back( trj );
  }

  if(!m_event->record( good_truthjet, "Good"+m_objKey ).isSuccess()) std::cout << "Could not record Good" << m_objKey << std::endl;
  if(!m_event->record( good_truthjet_aux, "Good" + m_objKey + "Aux." ).isSuccess()) std::cout << "Could not record Good" << m_objKey << "Aux" << std::endl;
}

//_________________________________________________________________________
//
void TruthJetHelper::Clear()
{
    // for(unsigned int i = 0; i < m_truthjet_vector.size(); ++i) delete m_truthjet_vector[i];
    // m_truthjet_vector.clear();
}
