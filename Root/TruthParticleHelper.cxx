#include <map>

#include "xAODBase/IParticleHelpers.h"

#include "MultibjetsAnalysis/TruthParticleHelper.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "MultibjetsAnalysis/HelperFunctions.h"

const double GEV = 1000;

//_________________________________________________________________________
//
TruthParticleHelper::TruthParticleHelper(xAOD::TEvent*& event, xAOD::TStore*& store) :
m_event(event),
m_store(store),
m_truthparticle_copy(0),
m_truthparticle_copy_aux(0),
m_truthparticle_vector(0)
{}

//_________________________________________________________________________
//
TruthParticleHelper::TruthParticleHelper( const TruthParticleHelper &t )
{
    m_event = t.m_event;
    m_store = t.m_store;
    m_truthparticle_copy = t.m_truthparticle_copy;
    m_truthparticle_copy_aux = t.m_truthparticle_copy_aux;
    m_truthparticle_vector = t.m_truthparticle_vector;
}

//_________________________________________________________________________
//
TruthParticleHelper::~TruthParticleHelper()
{}

//_________________________________________________________________________
//
void TruthParticleHelper::RetrieveTruthParticles()
{
    //Checks TruthParticle container exists
    const xAOD::TruthParticleContainer* truth_particles(0);
    if(!m_event->retrieve(truth_particles,"TruthParticles").isSuccess()){
        Warning( "TruthParticleHelper::RetrieveTruthParticles()", "Cannot retrieve TruthParticles containers !" );
        return;
    }

    std::pair<xAOD::TruthParticleContainer*,xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*truth_particles);
    m_truthparticle_copy = shallowcopy.first;
    m_truthparticle_copy_aux = shallowcopy.second;

    bool setLinks = xAOD::setOriginalObjectLink(*truth_particles, *m_truthparticle_copy);
    if(!setLinks) {
        Error("TruthParticleHelper::RetrieveTruthParticles()","Failed to set original object links on TruthParticle");
    }

    if(!m_store->record(m_truthparticle_copy, "TruthParts").isSuccess()) std::cout << "Could not record Truth Particles" << std::endl;
    if(!m_store->record(m_truthparticle_copy_aux, "TruthPartAux.").isSuccess()) std::cout << "Could not record Truth Particles Aux" << std::endl;
    return;
}

//_________________________________________________________________________
//
void TruthParticleHelper::GetLastChildren( const xAOD::TruthParticle *truthpart, std::set < int > &indices ){

    const int nChildren = truthpart -> nChildren();
    bool selfDecaying = false;
    const xAOD::TruthParticle *self_child(0);
    for ( int iChild = 0; iChild < nChildren; ++iChild ){
        const xAOD::TruthParticle *child = truthpart -> child( iChild );
        if( child && ( child -> pdgId() == truthpart -> pdgId() ) ){
            selfDecaying = true;
            self_child = child;
            break;
        }
    }
    if(selfDecaying){
        truthpart -> auxdecor < int > ("selfDecaying") = 1;
    } else {
        truthpart -> auxdecor < int > ("selfDecaying") = 0;
    }

    if( selfDecaying && self_child ){
        GetLastChildren( self_child, indices );
    } else if(!selfDecaying) {
        for ( int iChild = 0; iChild < nChildren; ++iChild ){
            const xAOD::TruthParticle *child = truthpart -> child( iChild );
            if(child) indices.insert( child -> index() );
        }
    }
}

//_________________________________________________________________________
//
void TruthParticleHelper::ReturnAllDescendantIndices( const xAOD::TruthParticle *truthpart, std::set < int > &indices ){

    //Get the vertex
    const xAOD::TruthVertex* decayVtx = truthpart -> decayVtx();
    if(!decayVtx) return;

    //Loop over outgoing particles
    const unsigned int nOutgoing = ((xAOD::TruthVertex*)decayVtx) -> nOutgoingParticles();
    for ( unsigned int iOut = 0; iOut < nOutgoing; ++iOut) {
        const xAOD::TruthParticle* child = (xAOD::TruthParticle*)decayVtx->outgoingParticle(iOut);
        if(!child) continue;
        indices.insert(child->index());
        ReturnAllDescendantIndices(child,indices);
    }
}

//_________________________________________________________________________
//
void TruthParticleHelper::ReturnAllAncestorsIndices( const xAOD::TruthParticle *truthpart, std::set < int > &indices ){

    const bool debug = false;

    if(debug){
        std::cout << "PDGId = " << truthpart->pdgId() << std::endl;
        std::cout << "   index = " << truthpart->index() << std::endl;
    }

    //Get the vertex
    const xAOD::TruthVertex* prodVtx = truthpart -> prodVtx();
    if(!prodVtx) return;

    //Loop over incoming particles
    const unsigned int nIncoming = ((xAOD::TruthVertex*)prodVtx) -> nIncomingParticles();
    for ( unsigned int iIn = 0; iIn < nIncoming; ++iIn) {
        const xAOD::TruthParticle* parent = (xAOD::TruthParticle*)prodVtx->incomingParticle(iIn);
        if(!parent) continue;
        indices.insert(parent->index());
        ReturnAllAncestorsIndices(parent,indices);
    }
}

//_________________________________________________________________________
//
bool TruthParticleHelper::IsInterestingPDGId( const unsigned int absPdgId )
{
    if(
       absPdgId==6 || //TOP
       absPdgId==7 || //BSM
       absPdgId==8 || //BSM
       (absPdgId>=1000001 && absPdgId<=1000039) || //BSM
       absPdgId==23 || //Z
       absPdgId==24 || //W
       absPdgId==25 //H
       ) return true;
    return false;
}

//_________________________________________________________________________
//
void TruthParticleHelper::RetrieveSignalTruthParticles()
{

    const bool debug = false;

    //
    // Selection based on kinematics or origin
    //
    std::set < int > vec_indices_to_keep;
    for( const auto & truthparticle_itr : *m_truthparticle_copy ) {

        // Interesting ?
        bool isInteresting = false;
        if ( IsInterestingPDGId( truthparticle_itr->absPdgId() ) && truthparticle_itr -> status() != 2 ){
            isInteresting = true;
        }

        if(debug){
            std::cout << "---> pdgId = " << truthparticle_itr -> pdgId();
            std::cout << "      index = " << truthparticle_itr -> index() << std::endl;
        }

        // Removes self-decayed particles from the list
        bool isSelfDecayed = false; //self-decayed = the current particle comes from a parent particle with same pdgId
        bool isComingFromInteresting = false; //immediate decay of interesting particles
        for ( unsigned int iParent = 0; iParent < truthparticle_itr -> nParents(); ++iParent) {
            const xAOD::TruthParticle *parent = truthparticle_itr -> parent( iParent );
            if(parent -> pdgId() == truthparticle_itr->pdgId()){
                isSelfDecayed = true;
                break;
            }
        }

        // Immediate decay of interesting particles
        if(!isSelfDecayed){
            for ( unsigned int iParent = 0; iParent < truthparticle_itr -> nParents(); ++iParent) {
                const xAOD::TruthParticle *parent = truthparticle_itr -> parent( iParent );
                if( IsInterestingPDGId( TMath::Abs(parent -> pdgId()) ) && parent -> status() != 2 ){
                    isComingFromInteresting = true;
                    break;
                }
            }
        }
        if( truthparticle_itr -> nParents() == 0 && truthparticle_itr -> absPdgId() != 6 && truthparticle_itr -> absPdgId() != 8 ){
            isComingFromInteresting = false;
            isInteresting = false;
        }

        // Final selection
        if( ( isInteresting || isComingFromInteresting ) && !isSelfDecayed ){
            vec_indices_to_keep.insert( truthparticle_itr -> index() );
            if (debug) {
                std::cout << "==> Truth particle with index: " << truthparticle_itr -> index() << std::endl;
                std::cout << "          - pdgId: " << truthparticle_itr -> pdgId() << std::endl;
                std::cout << "          - pT: " << truthparticle_itr -> pt() << std::endl;
                std::cout << "          - eta: " << truthparticle_itr -> eta() << std::endl;
                std::cout << "          - status: " << truthparticle_itr -> status() << std::endl;
                std::cout << "          - nChildren: " << truthparticle_itr -> nChildren() << std::endl;
                std::cout << "          - Children: ";
                for ( unsigned int iChild = 0; iChild < truthparticle_itr -> nChildren(); ++iChild) {
                    const xAOD::TruthParticle *child = truthparticle_itr -> child( iChild );
                    if(child) std::cout << child -> index() << ", ";
                }
                std::cout << std::endl;
                std::cout << "          - nParents: " << truthparticle_itr -> nParents() << std::endl;
                std::cout << "          - Parents: ";
                for ( unsigned int iParent = 0; iParent < truthparticle_itr -> nParents(); ++iParent) {
                    const xAOD::TruthParticle *parent = truthparticle_itr -> parent( iParent );
                    if(parent) std::cout << parent -> index() << ", ";
                }
                std::cout << std::endl;
            }
        }

    }

    //
    // Fills the map for index recomputation
    //
    int count_accepted = 0;
    std::map < int, int > index_map;
    for( const auto & truthparticle_itr : *m_truthparticle_copy ) {
        bool toKeep = false;
        for( auto itr : vec_indices_to_keep ){
            if(itr==static_cast<int>(truthparticle_itr->index())){
                toKeep = true;
                break;
            }
        }
        //
        //Fills map
        // -> new index if passes the selection
        // -> -1 if rejected
        if( toKeep ){
            index_map.insert( std::pair < int, int >(truthparticle_itr->index(), count_accepted) );
            count_accepted++;
        } else {
            index_map.insert( std::pair < int, int >(truthparticle_itr->index(), -1) );
        }
    }

    //
    // Loop over all truth particles and check on numbering scheme consistency (no cut at this level)
    // (avoid truth history break)
    //
    ConstDataVector<xAOD::TruthParticleContainer> * selected_truthpart   =  new ConstDataVector<xAOD::TruthParticleContainer>(SG::VIEW_ELEMENTS);

    for( const auto & truthparticle_itr : *m_truthparticle_copy ) {

        if(index_map[truthparticle_itr->index()]>-1){//don't try to keep the non-intersting particles

            //Saving the new index
            truthparticle_itr -> auxdecor < int > ("index_code") = index_map[truthparticle_itr->index()];

            //Collecting the children of the dropped self-decayed particles (if any)
            std::set < int > children_dropped;
            GetLastChildren( truthparticle_itr, children_dropped );

            int n_children = 0;
            if(children_dropped.size()==0){
                //Collecting informations about the children
                const xAOD::TruthVertex* decayVtx = truthparticle_itr -> decayVtx();
                if(decayVtx){
                    const unsigned int nOutgoing = ((xAOD::TruthVertex*)decayVtx) -> nOutgoingParticles();
                    for ( unsigned int iOut = 0; iOut < nOutgoing; ++iOut) {
                        const xAOD::TruthParticle* child = (xAOD::TruthParticle*)decayVtx->outgoingParticle(iOut);
                        if(!child) continue;
                        const int newIndex = index_map[child->index()];
                        if(newIndex!=-1){
                            n_children++;
                            std::string key = "child_index" + std::to_string(n_children);
                            truthparticle_itr -> auxdecor < int > (key) = newIndex;
                        }
                    }
                }
            } else {
                for ( const int child : children_dropped ) {
                    const int newIndex = index_map[child];
                    if(newIndex!=-1){
                        n_children++;
                        std::string key = "child_index" + std::to_string(n_children);
                        truthparticle_itr -> auxdecor < int > (key) = newIndex;
                    }
                }
            }
            truthparticle_itr -> auxdecor < int > ("children_n") = n_children;

            if(truthparticle_itr->pt()==0){
                truthparticle_itr -> setPx(0.);
                truthparticle_itr -> setPy(0.);
                truthparticle_itr -> setPz(0.);
                truthparticle_itr -> setE(0.);
            }

            //adding the decorated truth particle into the container
            selected_truthpart->push_back(truthparticle_itr);
        }
    }

    if(!m_store->record(selected_truthpart, "SelectedTruthParticles").isSuccess()) std::cout << "Could not record selected truth particles" << std::endl;
    m_truthparticle_vector = selected_truthpart -> asDataVector();

}

//_________________________________________________________________________
//
void TruthParticleHelper::MakeDeepCopy()
{
    const xAOD::TruthParticleContainer* selected_truthparticle(nullptr);
    if(!m_event->retrieve( selected_truthparticle, "SelectedTruthParticles" ).isSuccess()){
        std::cerr << "<!> Error in TruthParticleHelper::MakeDeepCopy() -> Cannot retrieve SelectedTruthParticles !" << std::endl;
        return;
    }
    const bool isSuccessCopy = HelperFunctions::makeDeepCopy<xAOD::TruthParticleContainer, xAOD::TruthParticleAuxContainer, xAOD::TruthParticle>(m_store, "FinalTruthParticles",selected_truthparticle);
    if(!isSuccessCopy){
        std::cerr << "<!> Error in TruthParticleHelper::MakeDeepCopy() -> Cannot do the DeepCopy" << std::endl;
    }

    const bool isSuccessRecord = HelperFunctions::recordOutput<xAOD::TruthParticleContainer, xAOD::TruthParticleAuxContainer>(m_event, m_store, "FinalTruthParticles");
    if(!isSuccessRecord) std::cerr << "<!> Error in TruthParticleHelper::MakeDeepCopy() -> Cannot do the record ..." << std::endl;
}

//_________________________________________________________________________
//
Float_t TruthParticleHelper::mtt(std::vector<float> mc_pt,
                                 std::vector<float> mc_eta,
                                 std::vector<float> mc_phi,
                                 std::vector<float> mc_pdgId,
                                 std::vector<float> mc_status)
{
    Float_t mass = 0;

    std::vector< TLorentzVector > tops;
    for(u_int i=0; i<mc_pt.size(); i++) {
        if(fabs(mc_pdgId.at(i))!=6) continue;
        if(mc_status.at(i)!=3) continue;

        TLorentzVector top;
        top.SetPtEtaPhiM(mc_pt.at(i),mc_eta.at(i),mc_phi.at(i),172.5);

        tops.push_back(top);
    }
    if(tops.size()!=2) {
        return mass;
    }

    mass = (tops.at(0)+tops.at(1)).M();
    return mass;
}

//_________________________________________________________________________
//
Float_t TruthParticleHelper::truth_met_filter()
{
    TLorentzVector neutrinoMET = TLorentzVector(0.,0.,0.,0.);
    for (const auto& particle:  *m_truthparticle_copy) {
        if (particle->isNeutrino()){
            auto particle_mother = particle->parent(0);
            if (particle_mother->nParents()>0 && particle_mother->parent(0)->isTop()) continue; //this is to avoid double-counting since there are usually two copies of the W in Powheg+Pythia6.
            if (particle_mother->isW() && particle_mother->nParents()==0){ //there are a few cases where the W has no parent.
                neutrinoMET+=particle->p4();
            }
            while (particle_mother->nParents()>0){
                if (particle_mother->parent(0)->isTop()) break;
                if (particle_mother->isW() && fabs(particle->parent(0)->parent(0)->pdgId()) < 100){ //get rid of semi-leptonic B decays with this cut.
                    neutrinoMET+=particle->p4();
                    break;
                }
                particle_mother = particle_mother->parent(0);
            }
        }
    }
    return neutrinoMET.Pt() / GEV;
}

//______________________________________________________________________________
//
Float_t TruthParticleHelper::truth_ht_filter(){
  // Get jet container out
  const xAOD::JetContainer* truthjets = 0;
  if ( m_event->retrieve( truthjets, "AntiKt4TruthWZJets").isFailure() || !truthjets ){
    Error( "TruthParticleHelper::truth_ht_filter()", "No xAOD::JetContainer found in TStore with key AntiKt4TruthWZJets" );
    return StatusCode::FAILURE;
  }

  // Get HT
  Float_t genFiltHT = 0.;
  for (const auto& tj : *truthjets) {
    if ( tj->pt()>35000 && fabs(tj->eta())<2.5 ) {
      genFiltHT += tj->pt();
    }
  }
  return genFiltHT / GEV;
}

//______________________________________________________________________________
//
int TruthParticleHelper::GetTopDecayMode( const bool doAntiTop ) const {

  int mask = 0;

  //
  // Determine whether there is a VLQ in the event
  //
  bool has7 = false;
  bool has8 = false;
  for (const auto& particle:  *m_truthparticle_copy) {
    if( (particle -> pdgId() == 8 && !doAntiTop) || (particle -> pdgId() == -8 && doAntiTop) ){
      has8 = true;
      break;
    }
    if( (particle -> pdgId() == 7 && !doAntiTop) || (particle -> pdgId() == -7 && doAntiTop) ){
      has7 = true;
      break;
    }
  }

  //
  // Determine the pdgId to look for
  //
  int index_to_look_for = doAntiTop ? -6 : 6;
  if(has7){
    index_to_look_for = doAntiTop ? -7 : 7;
  } else if(has8){
    index_to_look_for = doAntiTop ? -8 : 8;
  }

  for (const auto& particle:  *m_truthparticle_copy) {

    //Preselecting the truth particles to look into
    if( particle -> pdgId() != index_to_look_for ) continue;
    if( particle -> auxdecor < int > ("selfDecaying")==1 ) continue;

    //Getting the decay products
    const xAOD::TruthVertex* top_decayVtx = particle -> decayVtx();
    if(!top_decayVtx) continue;
    const unsigned int nOutgoing = ((xAOD::TruthVertex*)top_decayVtx) -> nOutgoingParticles();

    //Looping over the decay products
    for ( unsigned int iOut = 0; iOut < nOutgoing; ++iOut) {
      const xAOD::TruthParticle* top_child = (xAOD::TruthParticle*)top_decayVtx->outgoingParticle(iOut);
      if( !top_child ) continue;
      if( top_child->absPdgId() == 21 || top_child->absPdgId() == 22 ) continue;

      //Case of the VLQ decay identification
      if(has8 || has7){
        if(top_child->pdgId() == index_to_look_for) continue;
        mask |= 1<<TMath::Abs(top_child->pdgId());
      }
      //Case of the top decay type (hadronic or semi-leptonic top)
      else {
        if(TMath::Abs(top_child->pdgId())==24){
          const xAOD::TruthVertex* w_decayVtx = top_child -> decayVtx();
          if(!w_decayVtx) continue;
          const unsigned int nOutgoingFromW = ((xAOD::TruthVertex*)w_decayVtx) -> nOutgoingParticles();
          if(nOutgoingFromW == 2){
            for ( unsigned int iOutFromW = 0; iOutFromW < nOutgoingFromW; ++iOutFromW) {
              const xAOD::TruthParticle* w_child = (xAOD::TruthParticle*)w_decayVtx->outgoingParticle(iOutFromW);
              if(!w_child) continue;
              mask |= 1<<TMath::Abs(w_child->pdgId());
            }//decay products of W
          }//2 dcay products
        }//pdgId(W)
      }//case of top
    }//loop over decay products
  }
  return mask;
}
