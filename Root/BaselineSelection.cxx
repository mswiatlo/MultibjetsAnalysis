#include "MultibjetsAnalysis/BaselineSelection.h"

bool BaselineSelection::keep(
                bool doBaselineSelection,
                const xAOD::MissingETContainer* v_met,
				const xAOD::JetContainer* v_jets,
				const xAOD::MuonContainer* v_muons,
				const xAOD::ElectronContainer* v_electrons,
				int flag){

  bool result = false;

  xAOD::MissingETContainer::const_iterator met_it = v_met->find("Final");
  if (met_it == v_met->end()) std::cout << "No RefFinal inside MET container" << std::endl;
  xAOD::MissingET* met = *met_it;

  int muons_n = 0;
  for (auto muon : *v_muons) {
    if(muon->auxdata<char>("baseline")==0) continue;
    if(muon->auxdata<char>("passOR")==0) continue;
    if(muon->auxdata<char>("signal")==0) continue;
    muons_n ++;
  }

  int electrons_n = 0;
  for (auto electron : *v_electrons) {
    if(electron->auxdata<char>("baseline")==0) continue;
    if(electron->auxdata<char>("passOR")==0) continue;
    if(electron->auxdata<char>("signal")==0) continue;
    electrons_n ++;
  }

  int nb_jets_baseline = 0;
  int nb_jets_signal = 0;
  int nb_bjets = 0;

  for (auto jet : *v_jets) {
    if(jet->auxdata<char>("passOR")==0) continue;
    if(jet->auxdata<char>("baseline")==0) continue;
    nb_jets_baseline++;
    if(jet->pt() / MEV < 20.0){std::cout<<" Found baseline jet with pT < 20 GeV "<<std::endl;}

    if(jet->auxdata<char>("signal")==0) continue;
    nb_jets_signal++;
    if(jet->pt() / MEV < 25.0){std::cout<<" Found baseline jet with pT < 25 GeV "<<std::endl;}

    if(jet->auxdata<char>("bjet")==0) continue;
    if(fabs(jet->eta())>2.5) continue;
    nb_bjets ++;

    //if(jet->pt() / MEV < 20.0) continue;

    //if(jet->pt() / MEV > 30.0)  nb_jets_30++;

  }

  // ntuple + mini-xAOD outputs
  if(flag==0) {
    //if( met->met() / MEV > 200. && nb_jets_30>3 && muons_n==0 && electrons_n==0) result = true;
    if( met->met() / MEV > 200. && nb_jets_signal>3) result = true;
    if( muons_n > 0 && nb_jets_signal>3) result = true;
    if( electrons_n > 0 && nb_jets_signal>3) result = true;
  }
  // HF
  else if(flag==1) {
    if( met->met() / MEV > 150. && nb_jets_baseline>3 && nb_bjets>1) result = true;
  }

  return (!doBaselineSelection)||result;
}
