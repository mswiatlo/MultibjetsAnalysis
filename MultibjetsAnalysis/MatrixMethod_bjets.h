#ifndef MultibjetsAnalysis__MATRIXMETHOD_BJETS_H
#define MultibjetsAnalysis_MATRIXMETHOD_BJETS_H

#include "TObject.h"
#include "TH2F.h"
#include "TFile.h"
#include <TMath.h>
#include <iostream>
#include <utility>
#include <TMatrixD.h>
#include <vector>


class MatrixMethod_bjets 
{
public:
  MatrixMethod_bjets();
  ~MatrixMethod_bjets() {};
  virtual void initialize(std::string filename);

  float GetFakeEfficiency(float eta, float pt, int nJets, float mt, int syst);
    
  std::vector<double> GetWeight(std::vector<float> v_pt, std::vector<float> v_eta, std::vector<bool> v_isb, 
				std::vector<float> v_Beff, std::vector<float> v_Beff_down, std::vector<float> v_Beff_up, int nJets, float mt, float pt_cut);

private:
  static float m_etabins[3];
  static float m_ptbins[7];

  int Combination(int i, int ip);

  TFile * m_file;
  
  TH2F * h_eff_C[3];
  TH2F * h_eff_L[3];
  TH2F * h_eff_T[3];

  TH2F * h_frac_C_eta_pt[7];
  TH2F * h_frac_L_eta_pt[7];
  TH2F * h_frac_T_eta_pt[7];
    
  TH2F * h_frac_C_eta_pt_MCstat[7];
  TH2F * h_frac_L_eta_pt_MCstat[7];
  TH2F * h_frac_T_eta_pt_MCstat[7];
    
  TH1F * h_frac_C_mT;
  TH1F * h_frac_L_mT;
  TH1F * h_frac_T_mT;
    
  // fake rates from data
  TH2F* eff_fake_b[2];
  TH2F* eff_fake_b_stat[2];
};

#endif // not MATRIXMETHOD_BJETS_H

