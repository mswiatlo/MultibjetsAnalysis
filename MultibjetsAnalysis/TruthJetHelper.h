#ifndef MultibjetsAnalysis_TruthJetHelper_h
#define MultibjetsAnalysis_TruthJetHelper_h

#include <utility>
#include <string>

// xAOD
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "xAODCore/ShallowCopy.h"

#include "MultibjetsAnalysis/HelperFunctions.h"

class TruthJetHelper
{

 public:

  TruthJetHelper(xAOD::TEvent*& event, xAOD::TStore*& store);
  TruthJetHelper( const TruthJetHelper &t );
  virtual ~TruthJetHelper();

  // Retreive the truth jet container
  void RetrieveTruthJets();
  //fills the vector of truth Jets with the desired characteristics
  void RetrieveSignalTruthJets();
  // make deep copy of the truth Jets container in the output xAOD
  void MakeDeepCopy();
  // Clear to be called after execute
  void Clear();

  xAOD::JetContainer GetShallowCopy() {return *m_truthjet_copy;};
  const xAOD::JetContainer* GetSignalTruthJets() { return m_signal_truth_jets; };

  inline void SetObjKey( const std::string &objKey ) { m_objKey = objKey; }



 protected:

  xAOD::TEvent *m_event;  //!
  xAOD::TStore *m_store; //!

  // shallow copies
  xAOD::JetContainer*   m_truthjet_copy;
  xAOD::ShallowAuxContainer*    m_truthjet_copy_aux;

  // vector of truth Jets
  const xAOD::JetContainer*   m_signal_truth_jets;
 private:
    std::string m_objKey;

};

#endif //MultibjetsAnalysis_TruthJetHelper_h
