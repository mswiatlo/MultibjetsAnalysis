#ifndef MultibjetsAnalysis_BaselineSelection_H
#define MultibjetsAnalysis_BaselineSelection_H

#include <MultibjetsAnalysis/MuonHelper.h>
#include <MultibjetsAnalysis/ElectronHelper.h>
#include <MultibjetsAnalysis/JetHelper.h>
#include <xAODMissingET/MissingET.h>
#include "xAODMissingET/MissingETContainer.h"

class BaselineSelection {

 public:
  BaselineSelection();
  ~BaselineSelection();

  // the flag corresponds to the selection
  // 0: (met > 200 && >= 4 jets) || >= 1 signal lepton
  // 1: met > 150 && >= 4 jets && >= 2 b-jets with pt > 20 GeV (for HF inputs)


  static bool keep(
           bool doBaselineSelection,
           const xAOD::MissingETContainer* met,
		   const xAOD::JetContainer* v_jets,
		   const xAOD::MuonContainer* v_muons,
		   const xAOD::ElectronContainer*  v_electrons,
		   int flag);



};

#endif
