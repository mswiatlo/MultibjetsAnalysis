import glob
import ROOT
import os
#files = glob.glob("*/fetch/data-output_histfitter/*.root")
files = glob.glob("mc15_13TeV.407012.PowhegPythiaEvtGen_P2012CT10_ttbarMET200_hdamp172p5_nonAH.merge.DAOD_SUSY10.e4023_s2608_r6765_r6282_p2419_*.root")

cuts = [
    {"cut": "(baseline_electrons_n + baseline_muons_n)==0 && dphi_min>0.4 && pt_jet_4>90 && pt_bjet_3>90 && met>350 && meff_4j>1600", "name": "SR_Gbb_A"},
    {"cut": "(baseline_electrons_n + baseline_muons_n)==0 && dphi_min>0.4 && pt_jet_4>90 && pt_bjet_3>90 && met>450 && meff_4j>1400", "name": "SR_Gbb_B"},
    {"cut": "(baseline_electrons_n + baseline_muons_n)==0 && dphi_min>0.4 && pt_jet_4>30 && pt_bjet_3>30 && met>500 && meff_4j>1400", "name": "SR_Gbb_C"},
    {"cut": "(signal_electrons_n + signal_muons_n)>=1 && mT>150 && mTb_min>160 && jets_n>=6 && bjets_n>=3 && top_n>=1 && met>200 && meff_incl>1100", "name": "SR_Gtt_1l_A"},
    {"cut": "(signal_electrons_n + signal_muons_n)>=1 && mT>150 && mTb_min>160 && jets_n>=6 && bjets_n>=3 && top_n>=0 && met>300 && meff_incl>900", "name": "SR_Gtt_1l_B"},
    {"cut": "(signal_electrons_n + signal_muons_n)>=1 && mT>150 && mTb_min>0   && jets_n>=6 && bjets_n>=4 && top_n>=0 && met>250 && meff_incl>700", "name": "SR_Gtt_1l_C"},
    {"cut": "(signal_electrons_n + signal_muons_n)==0 && dphi_min>0.4 && mTb_min>80 && jets_n>=8 && bjets_n>=3 && top_n>=1 && met>400 && meff_incl>1700", "name": "SR_Gtt_0l_A"},
    {"cut": "(signal_electrons_n + signal_muons_n)==0 && dphi_min>0.4 && mTb_min>80 && jets_n>=8 && bjets_n>=4 && top_n>=1 && met>350 && meff_incl>1250", "name": "SR_Gtt_0l_B"},
    {"cut": "(signal_electrons_n + signal_muons_n)==0 && dphi_min>0.4 && mTb_min>80 && jets_n>=8 && bjets_n>=4 && top_n>=0 && met>350 && meff_incl>1250", "name": "SR_Gtt_0l_C"}
]

treenames = ["nominal",
         "EG_RESOLUTION_ALL__1down",
         "EG_RESOLUTION_ALL__1up",
         "EG_SCALE_ALL__1down",
         "EG_SCALE_ALL__1up",
         "JET_GroupedNP_1__1up",
         "JET_GroupedNP_1__1down",
         "JET_GroupedNP_2__1up",
         "JET_GroupedNP_2__1down",
         "JET_GroupedNP_3__1up",
         "JET_GroupedNP_3__1down",
         "JET_JER_SINGLE_NP__1up",
         "MUONS_ID__1down",
         "MUONS_ID__1up",
         "MUONS_MS__1down",
         "MUONS_MS__1up",
         "MUONS_SCALE__1down",
         "MUONS_SCALE__1up"]

for treename in treenames:
  print(treename)
  for f in files:
      print("\tInspecting {0:s}".format(os.path.basename(f)))
      f = ROOT.TFile.Open(f)
      t = f.Get(treename)
      print("\t\t{0:d} Entries".format(t.GetEntries()))
      print("\t\t\t{0:s}".format("\t".join(['{0:10s}'.format(cut['name']) for cut in cuts])))
      print("\t\t\t{0:s}".format("\t".join(['{0:10s}'.format('{0:d}'.format(t.GetEntries(cut['cut']))) for cut in cuts])))
